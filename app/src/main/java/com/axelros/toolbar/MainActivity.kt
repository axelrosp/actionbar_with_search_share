package com.axelros.toolbar

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.SearchView
import android.support.v7.widget.ShareActionProvider
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    var toolbar: Toolbar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar)
        toolbar?.setTitle(R.string.app_name)
        setSupportActionBar(toolbar)

        val btnTransaction = findViewById<Button>(R.id.btnTransaction)
        btnTransaction.setOnClickListener{
            val intent = Intent(this, PantallaDosActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)

        //Cargamos el item de busqueda del toolbar
        val itemSearch = menu?.findItem(R.id.btnSearch)
        var searchView = itemSearch?.actionView as SearchView
        //Share toolbar button
        val itemShare = menu?.findItem(R.id.share)
        val shareActionProvider = MenuItemCompat.getActionProvider(itemShare) as ShareActionProvider
        shareIntent(shareActionProvider)
        searchView.queryHint = "Escribe tu nombre..."

        searchView.setOnQueryTextFocusChangeListener{ view, hasFocus ->
            Log.d("LISTENERFOCUS", hasFocus.toString())
        }

        //Para trackear lo que escribe el usuario al momento
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            //Cada vez q el usuario introduce un caracter
            override fun onQueryTextChange(newText: String?): Boolean {
                Log.d("OnQueryTextCHange", newText)
                return true
            }

            //Cuan el usuario finaliza de escribir y le da a buscar resultado
            override fun onQueryTextSubmit(query: String?): Boolean {
                Log.d("OnQueryTextSubmit", query)
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.btnFav -> {
                Toast.makeText(this, "Favourite element", Toast.LENGTH_SHORT).show()
                return true
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }

    }

    private fun shareIntent(shareActionProvider: ShareActionProvider){
        if(shareActionProvider != null){
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_TEXT, "Este es un mensaje compartido")
            shareActionProvider.setShareIntent(intent)
        }
    }
}
